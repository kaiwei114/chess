from select import select
import socket
import types
import selectors

#Accepting new connection
def accept_wrapper(sock):
    conn, addr = sock.accept()
    print(f"Accepted connection from {addr}")
    conn.setblocking(False)
    data = types.SimpleNamespace(addr=addr, inb=b"", outb=b"")
    events = selectors.EVENT_READ | selectors.EVENT_WRITE
    sel.register(conn, events, data=data)

#Servicing existing connection (reading or writing)
def service_connection(key, mask):

    sock = key.fileobj
    data = key.data

    closed = False

    #Read and return message
    if mask & selectors.EVENT_READ:
        try:
            recv_data = sock.recv(1024)
            if recv_data:
                data.outb = recv_data 
                print(str(data.outb))
                return data.outb
            else:
                print(f"Closing connection to {data.addr}")
                sel.unregister(sock)
                sock.close()
                closed = True
        except:
            sel.unregister(sock)
            sock.close()
            closed = True

    #Write the previous message received
    if not closed and mask & selectors.EVENT_WRITE:
        b = last.encode('utf-8')
        sock.sendall(b)



#Listens on all IP addresses
HOST = ""
PORT = 65432

#Selector allows multiple connections
sel = selectors.DefaultSelector()

#Initial socket that listens for incoming connections
lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
lsock.bind((HOST,PORT))
lsock.listen()
print(f"Listening on {(HOST, PORT)}")
lsock.setblocking(False)
sel.register(lsock, selectors.EVENT_READ, data=None)

last = ""

#Event loop that listens on sel.select() for read/write requests
try:
    while True:
        try:
            events = sel.select(timeout = None)
            for key, mask in events:
                if key.data is None:    #If new connection
                    accept_wrapper(key.fileobj)
                else:                   #If existing connection
                    mes = service_connection(key, mask)
                    if not mes is None:
                        last = mes.decode("utf-8")
        except ConnectionResetError:
            print("Connection reset error.")
                
except KeyboardInterrupt:
    print("Keyboard interrupt")
finally:
    sel.close()

from multiprocessing.sharedctypes import Value
from pickle import EXT1
import pygame as pg
import graphics
from pieces import pawn, rook, bishop, queen, king, knight
import gamestate
import gamelogic as gl
import socket
import ipaddress

from tkinter import *
from tkinter import messagebox
import tkinter as tk

BOARD_INIT = "rnbqkbnr|pppppppp|eeeeeeee|eeeeeeee|eeeeeeee|eeeeeeee|PPPPPPPP|RNBQKBNR"

    
def render(screen):
    buffer = pg.Surface((screen.width, screen.height))


    pg.display.update()
    

def load_images():
    pieces = [pawn.pawn, rook.rook, bishop.bishop, queen.queen, king.king, knight.knight]
    for p in pieces:    
        p.ICON_WHITE = pg.transform.scale(pg.image.load(p.get_icon_file() + 'white.svg'), (100,100))
        p.ICON_BLACK = pg.transform.scale(pg.image.load(p.get_icon_file() + 'black.svg'), (100,100))
    


def print_board(board, surface, highlighted, bad_squares, init_col):

    square_size = 100

    log_console = False

    cols = [pg.Color(255, 204, 153), pg.Color(200, 100, 0)]
    for i in range(8):
        for j in range(8):
            col = cols[(i+j)%2]


            if (i,7-j) in highlighted:
                col = pg.Color(200, 200, 200)
            
            if (i,7-j) in bad_squares:
                col = pg.Color(200, 50, 50)
                if (i,7-j) in highlighted:
                    col = pg.Color(200, 50, 100)

            x = i * square_size
            y = j * square_size

            if init_col == 1:
                x = (7-i) * square_size
                y = (7-j) * square_size
                
            graphics.draw_rectangle(surface, x, y, square_size, square_size, col)


    rn = 0
    for row in reversed(board):
        cn = 0
        for square in row:
            if square == None:
                if log_console:
                    print('e', end =' ')
            else:
                if init_col == 0:
                    graphics.draw_piece(surface, square, rn, cn, square_size)
                elif init_col == 1:
                    graphics.draw_piece(surface, square, 7-rn, 7-cn, square_size)
                
                if log_console:
                    print(square.display_char(), end = ' ')
            #print(square, end=' ')
            cn += 1
        rn += 1
        if log_console:
            print()
    

def get_clicked_tile(mousePos, tile_width, init_col):
    mouseX = mousePos[0]
    mouseY = mousePos[1]

    tileX = int(mouseX / tile_width)
    if tileX > 7:
        return None

    tileY = int(mouseY / tile_width)
    if tileY > 7:
        return None

    if init_col == 0:
        return (tileX, 7 - tileY)
    else:
        return (7 - tileX, tileY)
    
    
HOST = None

#When ready to send message, send and then wait for opposing move
def send_message(messageStr, col, turnNumber):
    global HOST 
    
    if HOST is None:
        HOST = "127.0.0.1"
    
    PORT = 65432

    #Add s/e characters to the message to serve as start / end flags
    messageStr = 's' + messageStr + str(turnNumber%10) + 'e'
    responseLen = 6
    
    message = messageStr.encode('utf-8')

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))

        #Send the message detailing move made
        s.sendall(message)
        print("Sent message: " + str(message))
        
        #Initial response
        data = s.recv(1024)
        response = data.decode("utf-8")

        #Continually read server's response until it is of a valid form.
        while (len(response)<responseLen+2 or response[0]!='s' or response[7]!='e' or 
        response[5] == str(col) or int(response[6]) != (turnNumber+1)%10):
            data = s.recv(1024)
            response = data.decode("utf-8")

        #Send an empty message to force closure of the server's connection
        s.sendall(b'')
        s.close()
        print("Received: " + response)
        #Remove the s/e characters
        return response[1:responseLen+1]


input_ip = None
input_col = None
e1 = None
e2 = None
master = None

def process_input():
    print("Called")
    global input_ip, input_col

    input_ip =  e1.get()
    input_col = e2.get()

    if input_col in ["0", "1", "white", "black"]:
        if input_col == "white":
            input_col = 0
        if input_col == "black":
            input_col = 1

        try:
            ip = ipaddress.ip_address(input_ip)

            master.destroy()

        except:
            print("Invalid IP")
            messagebox.showinfo(title='Error',message="Invalid IP")
    else:
        messagebox.showinfo(title='Error',message="Invalid colour (enter 0 or 1)")
    



def init():

    global e1, e2, master, HOST

    #Initial config popup

    master = tk.Tk()
    Label(master, text="Server IP").grid(row=0)
    Label(master, text="Colour").grid(row=1)

    e1 = tk.Entry(master)
    e2 = tk.Entry(master)

    e1.insert(10, "127.0.0.1")
    e2.insert(10, "")

    e1.grid(row=0, column=1)
    e2.grid(row=1, column=1)

    tk.Button(master, 
          text='Confirm', 
          command=process_input).grid(row=3, 
                                    column=1, 
                                    sticky=tk.W, 
                                    pady=4)


    master.mainloop()

    HOST = input_ip

    start_col = 0
    
    try:
        start_col = int(input_col)
    except:
        start_col = 0

    print(BOARD_INIT)

    icon = pg.image.load('resources/icon.png')
    
    (width, height) = (800, 800)
    background_colour = (100, 100,100)

    tile_width = 100
    
    screen = pg.display.set_mode((width, height))

    pg.display.set_caption('Chess')
    pg.display.set_icon(icon)

    graphics.clear(screen, background_colour)

    pg.display.flip()
    
    running = True

    ###Game init
    load_images()


    state = gamestate.gamestate(0, BOARD_INIT)
    board = state.board
    
    print_board(board, screen, [], [], start_col)
    
    pg.display.update()

    
    ##Init server connection



    ##Init variables

    turn_number = 0

    current_turn = 0

    if start_col in [0,1]:
        current_turn = start_col

    
    highlighted = []
    selected = None

    bad_squares = []

    next = None

    #If playing as black, initially wait for white's first move
    
    if current_turn == 1:
        next = send_message("", current_turn, -1)
        current_turn = 0

    #Main game loop

    while running:
        for event in pg.event.get():
            if event.type == pg.QUIT:
                running = False
            elif not next is None or (event.type == pg.MOUSEBUTTONDOWN and event.button == 1): #1 = Left click, 2 = Right click, 3 = Scroll wheel
                #If it is your turn to move, set selected square via mouse click. Otherwise parse received move message
                if next is None:
                    tileX, tileY = get_clicked_tile(pg.mouse.get_pos(), tile_width, start_col)
                else:
                    selected = (int(next[0]), int(next[1]))
                    tileX = int(next[2])
                    tileY = int(next[3])
                    print(tileX)
                    print(tileY)
                    print(selected)
                
                #Check if the selected square results in a move
                if (tileX,tileY) in highlighted or not next is None:
                    
                    ##Detemine whether the move would put the current player in check:

                    check, (kingX, kingY) = gl.is_check(state, board, current_turn, ( (selected[0], selected[1]) , (tileX, tileY) ) )

                    if check:
                        bad_squares = [(tileX, tileY)]
                    else:
                        
                        #Make move

                        board[tileY][tileX] = board[selected[1]][selected[0]]
                        board[selected[1]][selected[0]] = None

                        #Pawn special behaviour
                        if board[tileY][tileX].type()=='pawn':
                            #Check if en-passant
                            if not state.prev_pawn_move is None:
                                if tileX==state.prev_pawn_move[0] and ( (current_turn == 0 and tileY == state.prev_pawn_move[1]+1) or (current_turn == 1 and tileY == state.prev_pawn_move[1]-1)):
                                    board[state.prev_pawn_move[1]][state.prev_pawn_move[0]] = None

                            state.prev_pawn_move = None
                            
                            #If double pawn move, assign last_pawn_move variable in gamestate to allow en-passant
                            if abs(tileY - selected[1]) == 2:
                                state.prev_pawn_move = (tileX, tileY)

                            if tileY == {0:7,1:0}[current_turn]:
                                board[tileY][tileX] = queen.queen(current_turn)
                        else:
                            state.prev_pawn_move = None

                        #King special behavioiur
                        if board[tileY][tileX].type()=='king':
                            #Check for long castling
                            if tileX == selected[0] - 2:
                                board[tileY][tileX+1] = board[tileY][0]
                                board[tileY][0] = None

                            #Check for short castling
                            if tileX == selected[0] + 2:
                                board[tileY][tileX-1] = board[tileY][7]
                                board[tileY][7] = None

                            
                        highlighted = []
                        bad_squares = []
                        
                        current_turn = 1 - current_turn #Switch between white and black turns
                        
                        #Determine whether the move puts the enemy in check

                        check, (kingX, kingY) = gl.is_check(state, board, current_turn, None)
                        if check:
                            #print("Check: " + str(check))
                            bad_squares = [(kingX, kingY)]
                            

                            #If the move put the enemy in check, determine whether this is checkmate
                            checkmate = gl.is_checkmate(state, board, current_turn, True)

                            if checkmate:
                                winner = {0:'White', 1:'Black'}[1-current_turn]
                                print("Checkmate! " + winner + " wins!")

                                print_board(board, screen, highlighted, bad_squares, start_col)
                                pg.display.update()
                                
                                Tk().wm_withdraw() #to hide the main window
                                messagebox.showinfo(title='Result',message=winner + " wins!")

                        #Send move if made locally
                        if next is None:
                            #Send move made
                            message = str(selected[0]) + str(selected[1]) + str(tileX) + str(tileY) + str(1-current_turn)
                            
                            print_board(board, screen, highlighted, bad_squares, start_col)
                            pg.display.update()
                            
                            next = send_message(message, 1-current_turn, turn_number)
                            
                        else:
                            next = None

                        turn_number += 1

                else:
                    #Display possible moves if a piece is selected. Otherwise clear all highlighted moves
                    if gl.occupied(board, tileX, tileY) and board[tileY][tileX].COL == current_turn:
                        #print(board[tileY][tileX].get_valid_moves(board,tileX, tileY))
                        highlighted = board[tileY][tileX].get_valid_moves(state,board,tileX, tileY)
                    else:
                        highlighted = []

                    selected = (tileX, tileY)
                
                print_board(board, screen, highlighted, bad_squares, start_col)
                pg.display.update()
                    



if __name__=="__main__":
    init()
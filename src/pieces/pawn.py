from pieces import piece

class pawn(piece.piece):

    def __init__(self, col):
        self.COL = col

    def display_char(self):
        if self.COL == 0:
            return 'p'
        else:
            return 'P'


    def get_valid_moves(self, state, board, x, y):
        
        moves = []

        #Board is given as row 0 = Rank 1
        flag = 1
        
        if self.COL ==0: #White
            flag = 1
        else:
            flag = -1

        if y+flag > 7 or y + flag < 0:
            return moves

        if board[y + flag][x] is None:
            moves.append((x, y+flag))
            
            #If on home squares, can move 2
            if self.COL == 0 and y==1 and board[y+2][x] is None:
                moves.append((x,y+2))

            if self.COL == 1 and y==6 and board[y-2][x] is None:
                moves.append((x,y-2))
                

        if x<7 and not (board[y+flag][x+1] is None) and board[y+flag][x+1].COL != self.COL:
            moves.append((x+1, y+flag))
        
        if x>0 and not (board[y+flag][x-1] is None) and board[y+flag][x-1].COL != self.COL:
            moves.append((x-1, y+flag))


        #Check if en-passant
        if not state.prev_pawn_move is None:
            if y == state.prev_pawn_move[1] and abs(x - state.prev_pawn_move[0]) == 1:
                moves.append((state.prev_pawn_move[0], y + flag))

        return moves


    def type(self):
        return 'pawn'

    def get_icon_file():
        return "resources/pieces/pawn/"

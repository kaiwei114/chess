from pieces import piece
import gamelogic as gl

class king(piece.piece):

    moved = False

    def __init__(self, col):
        self.COL = col

    def display_char(self):
        if self.COL == 0:
            return 'k'
        else:
            return 'K'

    def get_valid_moves(self, state, board, x, y):
        
        moves = []
        
        #down-left
        for i in range(1, min(min(x + 1,y + 1),2)):
            tx = x - i
            ty = y - i
            if gl.occupied(board, tx, ty):
                if board[ty][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, ty))
                    break
            else:
                moves.append((tx,ty))

        #down-right
        for i in range(1, min(min(7 -x + 1,y + 1),2)):
            tx = x + i
            ty = y - i
            if gl.occupied(board, tx, ty):
                if board[ty][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, ty))
                    break
            else:
                moves.append((tx,ty))

        #up-right
        for i in range(1, min(min(7 -x + 1,7 - y + 1),2)):
            tx = x + i
            ty = y + i
            if gl.occupied(board, tx, ty):
                if board[ty][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, ty))
                    break
            else:
                moves.append((tx,ty))

        #up-left
        for i in range(1, min(min(x + 1,7 - y + 1),2)):
            tx = x - i
            ty = y + i
            if gl.occupied(board, tx, ty):
                if board[ty][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, ty))
                    break
            else:
                moves.append((tx,ty))

        #left
        for tx in range(x-1, max(-1,x-2), -1):
            if gl.occupied(board, tx, y):
                if board[y][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, y))
                    break
            else:
                moves.append((tx,y))

        #right
        for tx in range(x+1, min(x+2,8), 1):
            if gl.occupied(board, tx, y):
                if board[y][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, y))
                    break
            else:
                moves.append((tx,y))
        
        #up
        for ty in range(y+1, min(y+2,8), 1):
            if gl.occupied(board, x, ty):
                if board[ty][x].COL == self.COL:
                    break
                else:
                    moves.append((x, ty))
                    break
            else:
                moves.append((x,ty))

        #down
        for ty in range(y-1, max(y-2,-1), -1):
            if gl.occupied(board, x, ty):
                if board[ty][x].COL == self.COL:
                    break
                else:
                    moves.append((x, ty))
                    break
            else:
                moves.append((x,ty))
        
        #Castling
        if self.moved == False and (not gl.is_check(state, board, self.COL, None)[0]):
            #Check if can castle long (left)
            if (not (gl.occupied(board, 1, y) or gl.occupied(board, 2 , y) or gl.occupied(board, 3, y)) 
                and gl.occupied(board,0,y) and board[y][0].type()=='rook' and board[y][0].moved==False):
                    moves.append((2, y))

            #Check if can castle short (right)
            if (not (gl.occupied(board, 5, y) or gl.occupied(board, 6 , y)) 
                and gl.occupied(board,7,y) and board[y][0].type()=='rook' and board[y][7].moved==False):
                    moves.append((6, y))
            
        return moves

    def type(self):
        return 'king'

    def get_icon_file():
        return "resources/pieces/king/"

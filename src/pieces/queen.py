from pieces import piece
import gamelogic as gl

class queen(piece.piece):

    def __init__(self, col):
        self.COL = col

    def display_char(self):
        if self.COL == 0:
            return 'q'
        else:
            return 'Q'

    def get_valid_moves(self, state, board, x, y):
        
        moves = []
        
        #down-left
        for i in range(1, min(x + 1,y + 1)):
            tx = x - i
            ty = y - i
            if gl.occupied(board, tx, ty):
                if board[ty][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, ty))
                    break
            else:
                moves.append((tx,ty))


        #down-right
        for i in range(1, min(7 -x + 1,y + 1)):
            tx = x + i
            ty = y - i
            if gl.occupied(board, tx, ty):
                if board[ty][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, ty))
                    break
            else:
                moves.append((tx,ty))

        #up-right
        for i in range(1, min(7 -x + 1,7 - y + 1)):
            tx = x + i
            ty = y + i
            if gl.occupied(board, tx, ty):
                if board[ty][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, ty))
                    break
            else:
                moves.append((tx,ty))

        #up-left
        for i in range(1, min(x + 1,7 - y + 1)):
            tx = x - i
            ty = y + i
            if gl.occupied(board, tx, ty):
                if board[ty][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, ty))
                    break
            else:
                moves.append((tx,ty))
        

        #left
        for tx in range(x-1,-1, -1):
            if gl.occupied(board, tx, y):
                if board[y][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, y))
                    break
            else:
                moves.append((tx,y))

        #right
        for tx in range(x+1, 8, 1):
            if gl.occupied(board, tx, y):
                if board[y][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, y))
                    break
            else:
                moves.append((tx,y))
        
        #up
        for ty in range(y+1, 8, 1):
            if gl.occupied(board, x, ty):
                if board[ty][x].COL == self.COL:
                    break
                else:
                    moves.append((x, ty))
                    break
            else:
                moves.append((x,ty))

        #down
        for ty in range(y-1, -1, -1):
            if gl.occupied(board, x, ty):
                if board[ty][x].COL == self.COL:
                    break
                else:
                    moves.append((x, ty))
                    break
            else:
                moves.append((x,ty))

        return moves



    def type(self):
        return 'queen'

    def get_icon_file():
        return "resources/pieces/queen/"

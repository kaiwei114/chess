from pieces import piece
import gamelogic as gl

class bishop(piece.piece):

    def __init__(self, col):
        self.COL = col

    def display_char(self):
        if self.COL == 0:
            return 'b'
        else:
            return 'B'

    def get_valid_moves(self, state, board, x, y):
        
        moves = []
        
        #down-left
        for i in range(1, min(x + 1,y + 1)):
            tx = x - i
            ty = y - i
            if gl.occupied(board, tx, ty):
                if board[ty][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, ty))
                    break
            else:
                moves.append((tx,ty))


        #down-right
        for i in range(1, min(7 -x + 1,y + 1)):
            tx = x + i
            ty = y - i
            if gl.occupied(board, tx, ty):
                if board[ty][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, ty))
                    break
            else:
                moves.append((tx,ty))

        #up-right
        for i in range(1, min(7 -x + 1,7 - y + 1)):
            tx = x + i
            ty = y + i
            if gl.occupied(board, tx, ty):
                if board[ty][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, ty))
                    break
            else:
                moves.append((tx,ty))

        #up-left
        for i in range(1, min(x + 1,7 - y + 1)):
            tx = x - i
            ty = y + i
            if gl.occupied(board, tx, ty):
                if board[ty][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, ty))
                    break
            else:
                moves.append((tx,ty))
        
        
        return moves



    def type(self):
        return 'bishop'


    def get_icon_file():
        return "resources/pieces/bishop/"

from pieces import piece
import gamelogic as gl

class rook(piece.piece):

    moved = False

    def __init__(self, col):
        self.COL = col

    def display_char(self):
        if self.COL == 0:
            return 'r'
        else:
            return 'R'

    def get_valid_moves(self, state, board, x, y):
        
        moves = []

        #left
        for tx in range(x-1,-1, -1):
            if gl.occupied(board, tx, y):
                if board[y][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, y))
                    break
            else:
                moves.append((tx,y))

        #right
        for tx in range(x+1, 8, 1):
            if gl.occupied(board, tx, y):
                if board[y][tx].COL == self.COL:
                    break
                else:
                    moves.append((tx, y))
                    break
            else:
                moves.append((tx,y))
        
        #up
        for ty in range(y+1, 8, 1):
            if gl.occupied(board, x, ty):
                if board[ty][x].COL == self.COL:
                    break
                else:
                    moves.append((x, ty))
                    break
            else:
                moves.append((x,ty))

        #down
        for ty in range(y-1, -1, -1):
            if gl.occupied(board, x, ty):
                if board[ty][x].COL == self.COL:
                    break
                else:
                    moves.append((x, ty))
                    break
            else:
                moves.append((x,ty))

        return moves



    def type(self):
        return 'rook'

    def get_icon_file():
        return "resources/pieces/rook/"

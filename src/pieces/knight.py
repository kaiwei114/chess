from pieces import piece
import gamelogic as gl

class knight(piece.piece):

    def __init__(self, col):
        self.COL = col

    def display_char(self):
        if self.COL == 0:
            return 'n'
        else:
            return 'N'

    def get_valid_moves(self, state, board, x, y):
        
        moves = []
        

        for xshift in [-1,1]:
            for yshift in [-2, 2]:
                tx = x+xshift
                ty = y+yshift
                if 0 <= tx <=7 and 0 <= ty <=7:
                    if not(gl.occupied(board, tx, ty) and board[ty][tx].COL == self.COL):
                        moves.append((tx,ty))

        for xshift in [-2,2]:
            for yshift in [-1, 1]:
                tx = x+xshift
                ty = y+yshift
                if 0 <= tx <=7 and 0 <= ty <=7:
                    if not(gl.occupied(board, tx, ty) and board[ty][tx].COL == self.COL):
                        moves.append((tx,ty))


        return moves



    def type(self):
        return 'knight'


    def get_icon_file():
        return "resources/pieces/knight/"

import pygame as pg

(WIDTH, HEIGHT) = (1000,1000)
BACKGROUND_COLOUR = (255,255,255)

    
def draw_rectangle(surface, left, top, width, height, colour):
    pg.draw.rect(surface, colour, pg.Rect(left, top, width, height))


def draw_piece(surface, piece, row, col, square_size):
    icon = None
    if piece.COL == 0:
        icon = piece.ICON_WHITE
    else:
        icon = piece.ICON_BLACK

    surface.blit(icon, (col*square_size, row*square_size))
    

def clear(screen, background_colour):
    screen.fill(background_colour)
    

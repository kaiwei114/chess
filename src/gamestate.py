from pieces import pawn, rook, knight, bishop, king, queen

class gamestate:
    
    board = None

    prev_pawn_move = None

    def init_board(self, board_rep):
        #Parse initial board string

        row = 0
        col = 0
        self.board = [[]]

        for c in board_rep:
            if c == '|':
                if len(self.board[row]) != 8:
                    raise Exception("Invalid board setup: " + str(len(self.board[row])) + " elements in row " + str(row))
                row += 1
                col = 0
                self.board.append([])
            else:
                if c== 'e':
                    self.board[row].append(None)
                else:
                    col = 0
                    if c.isupper():
                        col = 1
                    if c.lower()=='r':
                        self.board[row].append(rook.rook(col))
                    elif c.lower()=='b':
                        self.board[row].append(bishop.bishop(col))
                    elif c.lower()=='q':
                        self.board[row].append(queen.queen(col))
                    elif c.lower()=='k':
                        self.board[row].append(king.king(col))
                    elif c.lower()=='n':
                        self.board[row].append(knight.knight(col))
                    elif c.lower()=='p':
                        self.board[row].append(pawn.pawn(col))
                    else:
                        raise Exception("Invalid board setup: Unknown character " + c)
                
                #board[row].append(c)
                col += 1
                


    def __init__(self, startCol, board_rep):
        self.init_board(board_rep)
        
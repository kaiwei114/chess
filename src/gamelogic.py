#Pass in move as a ( (,) , (,) ) tuple 
#Can leave empty
#Checks if king of king_colour is in check

def is_check(state, board, king_colour, move):

    #Creates a (shallow) copy of the board. 
    #If move is provided, make this move on the cloned board
    new_board = board

    if not move is None:
        new_board = [row[:] for row in board]
        (oldX, oldY), (newX, newY) = move
        new_board[newY][newX] = new_board[oldY][oldX]
        new_board[oldY][oldX] = None

        #En-passant
        if new_board[newY][newX].type()=='pawn' and (not state.prev_pawn_move is None):
            if newX==state.prev_pawn_move[0] and ( 
                (king_colour == 0 and newY == state.prev_pawn_move[1]+1)
             or (king_colour == 1 and newY == state.prev_pawn_move[1]-1)):
                new_board[state.prev_pawn_move[1]][state.prev_pawn_move[0]] = None

        #Castling
        if new_board[newY][newX].type()=='king':
            #Check for long castling
            if newX == oldX - 2:
                new_board[newY][newX+1] = new_board[newX][0]
                new_board[newY][0] = None

            #Check for short castling
            if newX == oldX + 2:
                new_board[newY][newX-1] = new_board[newX][7]
                new_board[newY][7] = None
    
    #Get all of the pieces of the opposite colour
    enemies = get_pieces(new_board, 1-king_colour)
    
    #For each enemy piece, append their valid moves to possible_moves
    possible_moves = []
    for piece,x,y in enemies:
        if piece.type() != 'king':
            possible_moves += (piece.get_valid_moves(state, new_board, x, y))
    
    #Finally, check if the king's location is within possible_moves
    king_pos = get_king(new_board, king_colour)[1:]

    if king_pos in possible_moves:
        return (True, king_pos)
    else:
        return (False, king_pos)


#Determine whether king is in checkmate
def is_checkmate(state, board, king_colour, assume_check):
    #For efficiency, if assume_check = True then don't assess current position
    if (not assume_check) and (not is_check(state, board, king_colour)):
        return False

    #Obtain all pieces the same colour as the king
    king_pieces = get_pieces(board, king_colour)
    #Loop through every possible move that can be made by the king's colour's pieces
    #If all of these result in check, it is checkmate
    for piece,x,y in king_pieces:
        for move in piece.get_valid_moves(state, board, x, y):
            if not is_check(state, board, king_colour, ((x,y), move))[0]:
                return False

    return True

    
def occupied(board, x, y):
    return not board[y][x] is None


#Get list of all pieces of a particular color. Returns list of (piece, x ,y)

def get_pieces(board, colour):
    pieces = []
    for x in range(0,8):
        for y in range(0,8):
            if occupied(board, x, y) and board[y][x].COL == colour:
                pieces.append((board[y][x], x, y))
    return pieces


#Get the location of the king

def get_king(board, colour):
    for x in range(0,8):
        for y in range(0,8):
            if occupied(board, x, y) and board[y][x].COL == colour and board[y][x].display_char().lower() == 'k':
                return (board[y][x], x, y)
    